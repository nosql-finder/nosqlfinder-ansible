Steps to execute the playbook: :memo:
## 1. Installing Ansible
Install ansible thorough pip, if pip isn't in your system already, you can installed it with:
```
sudo easy_install pip
```
And to install Ansible:
```
sudo pip install ansible
```
More info about how to install Ansible for your operating system can be found on [Ansible Installation Guide.](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## 2. Clone the repo with the installation recipe
Clone the repo with the following command:
```
git clone https://gitlab.com/nosql-finder/nosqlfinder-ansible.git
```

### 3. Change some variables
1. Open the file: **hosts**
You'll see the following first files:
```
testserver ansible_host=<<LA IP DE TU SERVIDOR VA AQUI>> 
ansible_ssh_user=<<El USUARIO CON EL QUE TE CONECTAS AL SERVIDOR>> 
ansible_ssh_private_key_file=<< LA RUTA A TU LLAVE SSH PRIVADA >>
```
2. Replace the text `<<LA IP DE TU SERVIDOR VA AQUI>>` with the IP of the server where you'ld like to install the project.
3. Replace the text `<<El USUARIO CON EL QUE TE CONECTAS AL SERVIDOR>>` with the user that you use on your server, this is commonly **root**.
4. Replace the text `<< LA RUTA A TU LLAVE SSH PRIVADA >>` with the path to your private key. This playbook uses private key in order to be able to connect with the server and *we assume that the key doesn't have passphrase*.
5. Open the file: **nosqlfinder-ansible-vars.yml**
You'll see the following text:
```
neo4j_password: NEO4J_PASSWORD
google_email: YOUR GOOGLE EMAIL
google_password: YOUR GOOGLE PASSWORD
```
6. Replace `NEO4J_PASSWORD` with the password that you'ld like to use on the neo4j database.
7. Replace `YOUR GOOGLE EMAIL` for some email that you'ld like to use in order to use the google queries API and to send the emails from the contact page.
8. Replace `YOUR GOOGLE PASSWORD` with the password of the previous account. **If you don't want to give your password you could create an email account specific for this project.**

### 4. Run the playbook
```
ansible-playbook -i hosts playbook.yml --extra-vars "@nosqlfinder-ansible-vars.yml"
```

If you see an error with the message: `/usr/bin/python: not found` it means that your server doesn't have python installed (python is already on some linux versions), the error could look like this:
```
fatal: [testserver]: FAILED! => {"changed": false, "module_stderr": "Shared connection to 157.230.191.68 closed.\r\n", "module_stdout": "/bin/sh: 1: /usr/bin/python: not found\r\n", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 127}
```
To solve the error, install python on your server with:

```
sudo apt-get install python-minimal
```

### 5. Access the database 
You now can get access to the neo4j database pointing your ip on the browser and with the port 7474:
```
http://<YOUR API OR DOMAIN>:7474
```
You'll see the neo4j browser, replace on the **bolt** field the 0.0.0.0 by your ip.

![neo4j_browser.png](images/neo4j_browser.png)

### 6. See the tasks running
The initial script starts the tasks after 3 minutes of the hour when you played the playbook, wait the time and after that go to the luigi dashboard, who has all the data about the tasks, on your ip in the 8082 port.
```
http://<YOUR API OR DOMAIN>:8082
```
And if all it's correct you'll see the running tasks, the number of running task must be different from 0.

![luigi_browser.png](images/luigi_browser.png)

